#!/bin/bash

# some functions for colorized output
print_error() {
    echo -e "\033[01;31mERROR: ${1}\033[01;00m"
    if [[ -z $2 ]]; then
    	exit
    fi
}
print_good() {
    echo -e "\033[01;32m${1}\033[01;00m"
}

# exit with help if no URL given when executed
if [ -z "$2" ]; then
    print_good "Usage: $0 HEADER URL [URL2 ..]"
    exit 2
fi

# mac or linux?
sed --version > /dev/null 2>&1 /dev/null
if [[ $? -ne 0 ]]; then
	SED='gsed'
	`${SED} > /dev/null 2>&1 /dev/null`
	if [[ $? -eq 127 ]]; then
		print_error "Hey, you are using Mac OS, but no GNU Sed found in your PATH! You have to install it.\nTry this command: sudo port install gsed"
	fi
else
	SED='sed'
fi



isheader=0
regex="^(http:\/\/)?[a-zA-Z0-9_\-\.]+\/[a-zA-Z0-9_\.\/\-]*\?.*$"
HEADER=$1

# arguments iteration
for link in $@
do
	# pass first argument, because it is header
	if [[ isheader -lt 1 ]]; then
		isheader=1
		continue
	fi

	print_good "Processing \"${link}\""

	# check that string is a valid link
	if [[ ! $link =~ $regex ]]; then
		print_error "${link} is not a valid URL" no
		continue
	fi

	HNAME=`echo $link | $SED -nr "s|(http:\/\/)?([a-zA-Z0-9_\-\.]+)\/[a-zA-Z0-9_\.\/\-]*\?.*|\2|p"`

	echo -ne "\t"
	for i in `host $HNAME | grep 'has address' | awk '{print $4}'`;
	do
		LNK=`echo $link | $SED "s|${HNAME}|${i}|g"`
		RPL=`curl -I -s -H "${HEADER}: true" -H "Host: ${HNAME}" $LNK`
		RPLNUM=`echo $RPL | grep 'HTTP/' | cut -d' ' -f2`
		if [[ $RPLNUM -ne 200 ]]; then
			echo -n "(frontend $i returned code different from 200) "
		else
			echo -n "+ "
		fi
	done

	echo
done
